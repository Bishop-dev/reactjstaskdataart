var express = require('express')
  , bodyParser = require('body-parser')
  , chalk = require('chalk')
  , app = express()
  , directoriesRouter = require('./routes/directories')
  , noticesRouter = require('./routes/notices')
  , webpack = require('webpack')
  //, webpackDevMiddleware = require('webpack-dev-middleware')
  //, webpackHotMiddleware = require('webpack-hot-middleware')
  , config = require('./webpack.config')

//var compiler = webpack(config)
//app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }))
//app.use(webpackHotMiddleware(compiler))

app.use(express.static('public'))
app.use(bodyParser.json())

app.get('/', function (req, res) {
  res.sendFile('index.html', { root: 'public' })
})

app.use('/directories', directoriesRouter)
app.use('/notices', noticesRouter)

var server = app.listen(3000, function () {
  var port = server.address().port
  console.log('Server started at %s port', chalk.green(port))
})


/*
 var webpack = require('webpack');
 var WebpackDevServer = require('webpack-dev-server');
 var config = require('./webpack.config');

 new WebpackDevServer(webpack(config), {
 publicPath: config.output.publicPath,
 hot: true,
 historyApiFallback: true
 }).listen(3000, 'localhost', function (err, result) {
 if (err) {
 console.log(err);
 }

 console.log('Listening at localhost:3000');
 });
 */