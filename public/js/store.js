import {listen, emit} from './dispatcher';
import actions from './actions';
import './api';

const state = {};

const listeners = [];

export function getState() {
    return state;
}

export function addFetchDirectoriesListener(fn) {
    listeners.push(fn);
}

function notify() {
    listeners.forEach((fn) => fn());
}

listen(actions.FETCH_DIRECTORIES, () => {
   emit(actions.API_FETCH_DIRECTORIES);
});

listen(actions.FETCH_DIRECTORIES_SUCCESS, (data) => {
    state.directories = data;
    notify();
});

listen(actions.FETCH_DIRECTORIES_FAIL, (error) => {
    alert(error);
});