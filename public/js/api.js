import {listen, emit} from './dispatcher';
import actions from './actions';

listen(actions.FETCH_DIRECTORIES, () => {
    fetch('/directories', {
        method: 'GET'
    }).then(function(response) {
        emit(actions.FETCH_DIRECTORIES_SUCCESS, response.text);
    }).catch(function(error) {
        emit(actions.FETCH_DIRECTORIES_FAIL, error);
    });
});