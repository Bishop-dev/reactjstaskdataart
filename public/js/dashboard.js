import React from 'react';
import ReactDOM from 'react-dom';

import {emit} from './dispatcher';
import actions from './actions';
import {getState, addFetchDirectoriesListener} from './store';

var buttonsData = [
    {id: 1, label: "Add", src: "img/ic_note_add_black_48px.svg"},
    {id: 2, label: "Edit", src: "img/ic_mode_edit_black_48px.svg"},
    {id: 3, label: "Delete", src: "img/ic_delete_black_48px.svg"}
];

var folders = [
    {id: 1, name: "Corporative"},
    {id: 2, name: "Private"},
    {id: 3, parentId: 2, name: "Family"},
    {id: 4, parentId: 2, name: "Passwords"},
    {id: 5, name: "Other"}
];

const OPEN_FOLDER = 'img/ic_folder_open_black_48px.svg';
const CLOSED_FOLDER = 'img/ic_folder_black_48px.svg';

var ButtonBox = React.createClass({
    render: function() {
        return (
            <div className="img-with-text">
                <img src={this.props.src}/>
                <p>{this.props.label}</p>
            </div>
        );
    }
});

var SidebarPanel = React.createClass({
    render: function() {
        var buttons = this.props.buttons.map(function(btnData) {
            return (
                <ButtonBox key={btnData.id} src={btnData.src} label={btnData.label}/>
            );
        });
        return (
            <div className="buttons-set">
                {buttons}
            </div>
        );
    }
});

var FolderBox = React.createClass({
    render: function() {
        return (
            <div className="folder-with-text">
                <img src={CLOSED_FOLDER}/>
                <p>{this.props.folderData.name}</p>
            </div>
        );
    }
});

var FoldersPanel = React.createClass({
    getInitialState: function() {
        console.log('FoldersPanel:getInitialState');
        addFetchDirectoriesListener(this._fetch);
        return getState();
    },
    _fetch: function(data) {
        this.setProps({folders: data});
    },
    componentWillMount() {
        console.log('FoldersPanel:componentWillMount');
    },
    render: function() {
        var folders = this.props.folders.map(function(folderData) {
            return (
                <FolderBox key={folderData.id} folderData={folderData} />
            );
        });
        return (
            <div className="folders-set">
                {folders}
            </div>
        );
    }
});

var ManagerBox = React.createClass({
    render: function() {
        return (
            <div classID="mainBody">
                <SidebarPanel buttons={this.props.buttons}/>
                <FoldersPanel folders={this.props.folders} />
            </div>
        );
    }
});

ReactDOM.render(
    <ManagerBox buttons={buttonsData} />,
    document.getElementById('content')
);