var path = require('path');
var webpack = require('webpack');

var prodCfg = {
    //devtool: 'cheap-module-source-map',
    entry : [
        './public/js/dashboard.js'
    ],
    output: {
        path: path.join(__dirname, 'public/static'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loader: 'babel',
            exclude: /node_modules/,
            include: path.join(__dirname, 'public/js')
        }]
    },
    resolve: {
        extensions: ['', '.js'],
        modulesDirectories: ['node_modules', 'public/js']
    }
};

module.exports = prodCfg;