var idGenerator = require('./id-generator')

var store = {
  directories: [
    {id: 1, name: "Corporative"},
    {id: 2, name: "Private"},
    {id: 3, parentId: 2, name: "Family"},
    {id: 4, parentId: 2, name: "Passwords"},
    {id: 5, name: "Other"}
  ],
  notices: []
}

module.exports = store
